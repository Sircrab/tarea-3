package test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import sh4j.model.browser.SFactory;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

public class SFactoryTest {

    private SProject project;
    @Before
    public void setUp() throws IOException{
        project=(new SFactory()).create("./src");
    }
    @Test
    public void testPackage() {
        assertTrue(project.packages().size()>0);
    }
    @Test
    public void testClassses(){
        SPackage pkg=project.get("/test");
        assertTrue(pkg.classes().size()>0);
    }

}
