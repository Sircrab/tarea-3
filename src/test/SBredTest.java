package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import sh4j.model.highlight.*;
import sh4j.model.style.SBredStyle;
import sh4j.model.style.SDarkStyle;
import sh4j.parser.SASTParser;
import sh4j.parser.model.SBlock;

public class SBredTest {

    private SBlock source;
    
    @Before
    public void setUp(){
        
        source= SASTParser.parse("public class MainClass\n{\n   public static void main( String args[] )\n   { \n      GradeBook myGradeBook = new GradeBook(); \n\n      String courseName = \"Java \";\n      myGradeBook.displayMessage( courseName );\n   }\n\n}");
    }
    
    @Test
    public void testPseudoVariableDark() {
        String html=source.toHTML(new SDarkStyle(),
                new SClassName(),
                new SCurlyBracket(),
                new SKeyWord(),
                new SMainClass(),
                new SModifier(),
                new SPseudoVariable(),
                new SSemiColon(),
                new SString());
        assertEquals(html,"<pre style='color:#d1d1d1;background:#000000;'><span style='color:#e66170; font-weight:bold; '>public</span> <span style='color:#bb7977; '>class</span> MainClass <span style='color:#b060b0; '>{</span>\n    \n    <span style='color:#e66170; font-weight:bold; '>public</span> <span style='color:#bb7977; '>static</span> <span style='color:#bb7977; '>void</span> main(String args[])<span style='color:#b060b0; '>{</span>\n      GradeBook myGradeBook=<span style='color:#bb7977; '>new</span> GradeBook()<span style='color:#b060b0; '>;</span>\n      String courseName=<span style='color:#00c4c4; '>\"Java \"</span><span style='color:#b060b0; '>;</span>\n      myGradeBook.displayMessage(courseName)<span style='color:#b060b0; '>;</span>\n      \n    <span style='color:#b060b0; '>}</span>\n  <span style='color:#b060b0; '>}</span></pre>");
    }
    
    @Test
    public void testClassName() {
        String html=source.toHTML(new SBredStyle(),new SClassName());
        assertEquals(html,"<pre style='color:#000000;background:#f1f0f0;'>public class MainClass {\n    \n    public static void main(String args[]){\n      GradeBook myGradeBook=new GradeBook();\n      String courseName=\"Java \";\n      myGradeBook.displayMessage(courseName);\n      \n    }\n  }</pre>");
        
    }
    @Test
    public void testMainClass() {
        String html=source.toHTML(new SBredStyle(),new SMainClass());
        assertEquals(html,"<pre style='color:#000000;background:#f1f0f0;'>public class MainClass {\n    \n    public static void main(<span style='color:#800040; '>String</span> args[]){\n      GradeBook myGradeBook=new GradeBook();\n      <span style='color:#800040; '>String</span> courseName=\"Java \";\n      myGradeBook.displayMessage(courseName);\n      \n    }\n  }</pre>");
        
    }
    @Test
    public void testCurlyBracket() {
        String html=source.toHTML(new SBredStyle(),new SCurlyBracket());
        assertEquals(html,"<pre style='color:#000000;background:#f1f0f0;'>public class MainClass <span style='color:#806030; '>{</span>\n    \n    public static void main(String args[])<span style='color:#806030; '>{</span>\n      GradeBook myGradeBook=new GradeBook();\n      String courseName=\"Java \";\n      myGradeBook.displayMessage(courseName);\n      \n    <span style='color:#806030; '>}</span>\n  <span style='color:#806030; '>}</span></pre>");
        
    }
    @Test
    public void testKeyWord() {
        String html=source.toHTML(new SBredStyle(),new SKeyWord());
        assertEquals(html,"<pre style='color:#000000;background:#f1f0f0;'>public <span style='color:#800040; '>class</span> MainClass {\n    \n    public <span style='color:#800040; '>static</span> <span style='color:#800040; '>void</span> main(String args[]){\n      GradeBook myGradeBook=<span style='color:#800040; '>new</span> GradeBook();\n      String courseName=\"Java \";\n      myGradeBook.displayMessage(courseName);\n      \n    }\n  }</pre>");
        
    }
    @Test
    public void testModifier() {
        String html=source.toHTML(new SBredStyle(),new SModifier());
        assertEquals(html,"<pre style='color:#000000;background:#f1f0f0;'><span style='color:#400000; font-weight:bold; '>public</span> class MainClass {\n    \n    <span style='color:#400000; font-weight:bold; '>public</span> static void main(String args[]){\n      GradeBook myGradeBook=new GradeBook();\n      String courseName=\"Java \";\n      myGradeBook.displayMessage(courseName);\n      \n    }\n  }</pre>");
        
    }
    @Test
    public void testPseudoVariable() {
        String html=source.toHTML(new SBredStyle(),new SPseudoVariable());
        assertEquals(html,"<pre style='color:#000000;background:#f1f0f0;'>public class MainClass {\n    \n    public static void main(String args[]){\n      GradeBook myGradeBook=new GradeBook();\n      String courseName=\"Java \";\n      myGradeBook.displayMessage(courseName);\n      \n    }\n  }</pre>");
        
    }
    @Test
    public void testSemiColon() {
        String html=source.toHTML(new SBredStyle(),new SSemiColon());
        assertEquals(html,"<pre style='color:#000000;background:#f1f0f0;'>public class MainClass {\n    \n    public static void main(String args[]){\n      GradeBook myGradeBook=new GradeBook()<span style='color:#806030; '>;</span>\n      String courseName=\"Java \"<span style='color:#806030; '>;</span>\n      myGradeBook.displayMessage(courseName)<span style='color:#806030; '>;</span>\n      \n    }\n  }</pre>");
        
    }
    @Test
    public void testString() {
        String html=source.toHTML(new SBredStyle(),new SString());
        assertEquals(html,"<pre style='color:#000000;background:#f1f0f0;'>public class MainClass {\n    \n    public static void main(String args[]){\n      GradeBook myGradeBook=new GradeBook();\n      String courseName=<span style='color:#e60000; '>\"Java \"</span>;\n      myGradeBook.displayMessage(courseName);\n      \n    }\n  }</pre>");
        
    }
    @Test
    public void testtoString(){
        assertEquals(new SBredStyle().toString(),"bred");
    }
    
}
