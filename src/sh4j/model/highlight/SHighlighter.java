package sh4j.model.highlight;

import sh4j.model.style.SStyle;
/**
 * Interface that all highlatable objects must comply.
 * @author sircrab
 *
 */
public interface SHighlighter {
  public boolean needsHighLight(String text);

  public String highlight(String text, SStyle style);
}
