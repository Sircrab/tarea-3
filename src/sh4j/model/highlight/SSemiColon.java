package sh4j.model.highlight;

import sh4j.model.style.SStyle;


/**
 * Class which represents a semicolon(;) highlatable object.
 * @author sircrab
 *
 */
public class SSemiColon implements SHighlighter {

  @Override
  public boolean needsHighLight(String text) {
    return ";".equals(text);
  }

  @Override
  public String highlight(String text, SStyle style) {
    return style.formatSemiColon(text);
  }

}
