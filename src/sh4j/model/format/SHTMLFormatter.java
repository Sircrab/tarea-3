package sh4j.model.format;

import sh4j.model.highlight.SDummy;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SStyle;
import sh4j.parser.model.SBlock;
import sh4j.parser.model.SText;
/**
 * Class which represents an HTMLformatter, takes in styles and returns HTML text.
 * @author sircrab
 *
 */
public class SHTMLFormatter implements SFormatter {
  private final StringBuffer buffer;
  private int level;
  private final SStyle style;
  private final SHighlighter[] highlighters;
  /**
   * Constructor for HtmlFormatter.
   * @param style Style to use while formatting
   * @param hs Objects which will be highlighted
   */
  public SHTMLFormatter(SStyle style, SHighlighter... hs) {
    this.style = style;
    highlighters = hs;
    buffer = new StringBuffer();
  }

  private SHighlighter lookup(String text) {
    for (SHighlighter h : highlighters) {
      if (h.needsHighLight(text)) {
        return h;
      }
    }
    return new SDummy();
  }

  @Override
  public void styledWord(String word) {
    buffer.append(lookup(word).highlight(word, style));
  }

  @Override
  public void styledChar(char letter) {
    buffer.append(lookup(letter + "").highlight(letter + "", style));
  }

  @Override
  public void styledSpace() {
    buffer.append(' ');
  }

  @Override
  public void styledCR() {
    buffer.append("\n");
    indent();
  }

  @Override
  public void styledBlock(SBlock block) {
    level++;
    for (SText text : block.texts()) {
      text.export(this);
    }
    level--;
  }
  /**
   * Indenting method, simply adds a tab.
   */
  public void indent() {
    for (int i = 0; i < level; i++) {
      buffer.append("  ");
    }

  }

  @Override
  public String formattedText() {
    return style.formatBody(buffer.toString());
  }

  public static String tag(String name, String content, String style) {
    return "<" + name + " style='" + style + "'>" + content + "</" + name + ">";
  }
}
