package sh4j.model.format;

import sh4j.parser.model.SBlock;
/**
 * Interface that all formatter objects must comply.
 * @author sircrab
 *
 */
public interface SFormatter {
  public void styledWord(String word);

  public void styledChar(char letter);

  public void styledSpace();

  public void styledCR();

  public void styledBlock(SBlock block);

  public String formattedText();
}
