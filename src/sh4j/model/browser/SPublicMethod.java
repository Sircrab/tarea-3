package sh4j.model.browser;

import org.eclipse.jdt.core.dom.MethodDeclaration;

import sh4j.parser.model.SBlock;
/**
 * Class which represents a public method.
 * @author sircrab
 *
 */
public class SPublicMethod extends SMethod {
  public SPublicMethod(MethodDeclaration node, SBlock body) {
    super(node, body);
  }

  @Override
  public String icon() {
    return "./resources/public_co.gif";
  }

}
