package sh4j.model.browser;

import org.eclipse.jdt.core.dom.TypeDeclaration;
import sh4j.model.command.SSortClassesByHierarchyVisitor;
import sh4j.model.command.SSortClassesByNameVisitor;
import sh4j.model.command.SSortPackagesByNameVisitor;

import java.awt.Color;
import java.awt.Font;
import java.util.List;


/**
 * Class for "Classes" in the Browser. Can be either an interface or not.
 * 
 * @author sircrab
 *
 */
public class SClass implements SObject {
  private final TypeDeclaration declaration;
  private final List<SMethod> methods;
  protected int level = 0;

  public SClass(TypeDeclaration td, List<SMethod> ms) {
    declaration = td;
    methods = ms;
  }

  public List<SMethod> methods() {
    return methods;
  }

  public String className() {
    return declaration.getName().getIdentifier();
  }

  /**
   * Method to check if a class it's an interface or not.
   * 
   * @return true or false if it's interface or not.
   */
  public boolean isInterface() {
    return declaration.isInterface();
  }

  /**
   * Method which returns the superclass of an object.
   * 
   * @return A string which represents the superClass of this class.
   */
  public String superClass() {
    if (declaration.getSuperclassType() == null) {
      return "Object";
    }
    return declaration.getSuperclassType().toString();
  }

  public String toString() {
    return className();
  }

  @Override
  public String icon() {
    if (isInterface()) {
      return "./resources/int_obj.gif";
    }
    return "./resources/class_obj.gif";
  }

  @Override
  public Font font() {
    if (isInterface()) {
      return new Font("Helvetica", Font.ITALIC, 12);
    }
    return new Font("Helvetica", Font.PLAIN, 12);
  }



  @Override
  /**
   * Visitor acceptor for class, it is just a dummy for now.
   */
  public void accept(SSortPackagesByNameVisitor visitor) {
    // Dummy Line
  }


  @Override
  /**
   * Visitor acceptor for class, it is just a dummy for now.
   */
  public void accept(SSortClassesByNameVisitor visitor) {
    // Dummy Line
  }


  @Override
  /**
   * Visitor acceptor for class, it is just a dummy for now.
   */
  public void accept(SSortClassesByHierarchyVisitor visitor) {
    // Dummy Line
  }

  @Override
  public Color background() {
    return Color.WHITE;
  }

  @Override
  public int level() {
    return level;
  }
}
