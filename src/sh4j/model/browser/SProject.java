package sh4j.model.browser;

import sh4j.model.command.SSortClassesByHierarchyVisitor;
import sh4j.model.command.SSortClassesByNameVisitor;
import sh4j.model.command.SSortPackagesByNameVisitor;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


/**
 * Class which represents a Project inside the browser.
 * 
 * @author sircrab
 *
 */
public class SProject implements SObject {
  private final List<SPackage> packages;

  public SProject() {
    packages = new ArrayList<SPackage>();
  }

  public void addPackage(SPackage pack) {
    packages.add(pack);
  }

  /**
   * Method that returns the packages.
   * 
   * @return A list of Spackages.
   */
  public List<SPackage> packages() {
    return packages;
  }

  /**
   * Method that returns a package by name.
   * 
   * @param pkgName name of the package to return.
   * @return an Spackage by that name or null.
   */
  public SPackage get(String pkgName) {
    for (SPackage pkg : packages) {
      if (pkg.toString().equals(pkgName)) {
        return pkg;
      }
    }
    return null;
  }

  @Override
  public String icon() {
    // No icon for project
    return null;
  }

  @Override
  public void accept(SSortPackagesByNameVisitor visitor) {
    packages.sort(new Comparator<SPackage>() {

      @Override
      public int compare(SPackage o1, SPackage o2) {
        return o1.toString().compareTo(o2.toString());
      }


    });


  }

  @Override
  public void accept(SSortClassesByNameVisitor visitor) {
    // Propagate
    for (SPackage pack : packages) {
      visitor.visit(pack);
    }

  }

  @Override
  public void accept(SSortClassesByHierarchyVisitor visitor) {
    for (SPackage pack : packages) {
      visitor.visit(pack);
    }


  }

  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  @Override
  public Color background() {
    return Color.WHITE;
  }

  @Override
  public int level() {
    return 0;
  }

}
