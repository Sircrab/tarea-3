package sh4j.model.browser;

import sh4j.model.command.SSortClassesByHierarchyVisitor;
import sh4j.model.command.SSortClassesByNameVisitor;
import sh4j.model.command.SSortPackagesByNameVisitor;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class which represents objects which are "Packages" in the browser.
 * 
 * @author sircrab
 *
 */
public class SPackage implements SObject {

  private final String name;
  private List<SClass> classes;

  public SPackage(String name) {
    classes = new ArrayList<SClass>();
    this.name = name;
  }

  public void addClass(SClass cls) {
    classes.add(cls);
  }

  public List<SClass> classes() {
    return classes;
  }

  public String toString() {
    return name;
  }

  @Override
  public String icon() {
    if (classes.isEmpty()) {
      return "./resources/pack_empty_co.gif";
    }
    return "./resources/package_mode.gif";
  }


  @Override
  /**
   * Visitor acceptor for Packages. For this visitor it is just a Dummy.
   */
  public void accept(SSortPackagesByNameVisitor visitor) {
    for (SClass cls : classes) {
      cls.level = 0;
    }
  }


  @Override
  /**
   * Visitor acceptor for Packages. Regular ascending sort by string.
   */
  public void accept(SSortClassesByNameVisitor visitor) {
    classes.sort(new Comparator<SClass>() {

      @Override
      public int compare(SClass o1, SClass o2) {
        return o1.toString().compareTo(o2.toString());
      }

    });
    for (SClass cls : classes) {
      cls.level = 0;
    }

  }


  @Override
  /**
   * Visitor acceptor for Packages. O(N^2) Hierarchy sort by insertion.
   */
  public void accept(SSortClassesByHierarchyVisitor visitor) {
    Map<String, ArrayList<SClass>> map = new HashMap<String, ArrayList<SClass>>();
    for (SClass cls : classes) {
      map.put(cls.toString(), new ArrayList<SClass>());
    }
    map.put("Object", new ArrayList<SClass>());
    ArrayList<SClass> sortedClasses = new ArrayList<SClass>();
    for (int i = 0; i < classes.size(); i++) {
      if (classes.get(i).superClass().equals("Object")) {
        map.get("Object").add(classes.get(i));
      }
    }
    for (int i = 0; i < classes.size(); i++) {
      for (int j = 0; j < classes.size(); j++) {
        if (classes.get(i).toString().equals(classes.get(j).superClass())) {
          map.get(classes.get(i).toString()).add(classes.get(j));
        }
      }
    }
    SClassDummy dummy = new SClassDummy();
    preOrder(dummy, map, sortedClasses, -1);
    classes = sortedClasses;



  }

  private void preOrder(SClass cur, Map<String, ArrayList<SClass>> map, ArrayList<SClass> obj,
      int level) {
    if (!cur.toString().equals("Object")) {
      cur.level = level;
      obj.add(cur);
    }

    for (SClass cls : map.get(cur.toString())) {
      preOrder(cls, map, obj, level + 1);
    }
  }


  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  @Override
  public Color background() {
    return Color.WHITE;
  }

  @Override
  public int level() {
    return 0;
  }


}
