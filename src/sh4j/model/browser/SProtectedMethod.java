package sh4j.model.browser;

import org.eclipse.jdt.core.dom.MethodDeclaration;

import sh4j.parser.model.SBlock;
/**
 * Class which representes a private method.
 * @author sircrab
 *
 */
public class SProtectedMethod extends SMethod {
  public SProtectedMethod(MethodDeclaration node, SBlock body) {
    super(node, body);
  }

  @Override
  public String icon() {
    return "./resources/protected_co.gif";
  }

}
