package sh4j.model.browser;

import sh4j.model.command.SSortClassesByHierarchyVisitor;
import sh4j.model.command.SSortClassesByNameVisitor;
import sh4j.model.command.SSortPackagesByNameVisitor;

import java.awt.Color;
import java.awt.Font;


/**
 * Interface that all objects must comply.
 * 
 * @author sircrab
 *
 */
public interface SObject {
  String icon();

  Font font();

  Color background();
  
  int level();

  void accept(SSortPackagesByNameVisitor visitor);

  void accept(SSortClassesByNameVisitor visitor);

  void accept(SSortClassesByHierarchyVisitor visitor);
}
