package sh4j.model.browser;

import org.eclipse.jdt.core.dom.MethodDeclaration;

import sh4j.parser.model.SBlock;
/**
 * Class that representes a private method.
 * @author sircrab
 *
 */
public class SPrivateMethod extends SMethod {
  public SPrivateMethod(MethodDeclaration node, SBlock body) {
    super(node, body);
  }

  @Override
  public String icon() {
    return "./resources/private_co.gif";
  }

}
