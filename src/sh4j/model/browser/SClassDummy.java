package sh4j.model.browser;

/**
 * Dummy class for SClass.
 * @author sircrab
 *
 */

public class SClassDummy extends SClass {
  /**
   * Default Constructor.
   */
  public SClassDummy() {
    super(null, null);
  }
  /**
   * This makes it a dummy, it always returns "Object".
   */
  public String toString() {
    return "Object";
  }

}
