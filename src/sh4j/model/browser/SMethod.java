package sh4j.model.browser;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import sh4j.model.command.SSortClassesByHierarchyVisitor;
import sh4j.model.command.SSortClassesByNameVisitor;
import sh4j.model.command.SSortPackagesByNameVisitor;
import sh4j.parser.model.SBlock;

import java.awt.Color;
import java.awt.Font;



/**
 * Class Which represents a method in the browser. Contained in a Class.
 * 
 * @author sircrab
 *
 */
public class SMethod implements SObject {
  private final MethodDeclaration declaration;
  private final SBlock body;

  public SMethod(MethodDeclaration node, SBlock body) {
    declaration = node;
    this.body = body;
  }

  /**
   * Method for obtaining the number of LOC in the method.
   * 
   * @return LOC in the method.
   */
  public int getLinesOfCode() {
    String code = body.toString();
    return code.length() - code.replace("\n", "").length() + 1;
  }
  /**
   * Method which returns the modifier to a method (public,private,etc).
   * @return Returns a string with the modifier of this method.
   */
  public String modifier() {
    for (Object obj : declaration.modifiers()) {
      if (obj instanceof Modifier) {
        Modifier modifier = (Modifier) obj;
        return modifier.getKeyword().toString();
      }
    }
    return "default";
  }

  public String name() {
    return declaration.getName().getIdentifier();
  }

  public SBlock body() {
    return body;
  }

  public String toString() {
    return name();
  }

  @Override
  public String icon() {
    // default value
    return "./resources/default_co.gif";
  }

  @Override
  public Color background() {
    int count = getLinesOfCode();
    Color color = Color.WHITE;
    if (count > 30) {
      color = Color.YELLOW;
    }
    if (count > 50) {
      color = Color.RED;
    }
    return color;
  }


  @Override
  /**
   * Visitor acceptor for Methods. It currently it's just a Dummy.
   */
  public void accept(SSortPackagesByNameVisitor visitor) {
    // Dummy Line
  }


  @Override
  /**
   * Visitor acceptor for Methods. It currently it's just a Dummy.
   */
  public void accept(SSortClassesByNameVisitor visitor) {
    // Dummy Line
  }


  @Override
  /**
   * Visitor acceptor for Methods. It currently it's just a Dummy.
   */
  public void accept(SSortClassesByHierarchyVisitor visitor) {
    // Dummy Line
  }

  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  @Override
  public int level() {
    return 0;
  }



}
