package sh4j.model.style;

import sh4j.ui.IMenuItem;
import sh4j.ui.SFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;



/**
 * Interface that all Styles must comply.
 * 
 * @author sircrab
 *
 */
public abstract class SStyle implements IMenuItem {

  public abstract String toString();

  public abstract String formatClassName(String text);

  public abstract String formatCurlyBracket(String text);

  public abstract String formatKeyWord(String text);

  public abstract String formatPseudoVariable(String text);

  public abstract String formatSemiColon(String text);

  public abstract String formatString(String text);

  public abstract String formatMainClass(String text);

  public abstract String formatModifier(String text);

  public abstract String formatBody(String text);

  /**
   * This method is used to add this Style to the Styles menu in the SFrame.
   */
  public void addToMenu(SFrame frame) {
    JMenuItem item = new JMenuItem(this.toString());
    item.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent event) {
        frame.style(SStyle.this);
      }
    });
    frame.styleFile.add(item);
  }


}
