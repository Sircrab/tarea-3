package sh4j.model;

import sh4j.model.command.SSortClassesByHierarchy;
import sh4j.model.command.SSortClassesByName;
import sh4j.model.command.SSortPackagesByName;
import sh4j.model.highlight.SClassName;
import sh4j.model.highlight.SCurlyBracket;
import sh4j.model.highlight.SKeyWord;
import sh4j.model.highlight.SMainClass;
import sh4j.model.highlight.SModifier;
import sh4j.model.highlight.SPseudoVariable;
import sh4j.model.highlight.SSemiColon;
import sh4j.model.highlight.SString;
import sh4j.model.style.SBredStyle;
import sh4j.model.style.SDarkStyle;
import sh4j.model.style.SEclipseStyle;
import sh4j.ui.SFrame;

import java.io.IOException;

/**
 * Regular MainClass entry.
 * 
 * @author sircrab
 *
 */
public final class MainClass {
  private MainClass() {
    // This is a dummy line
  }

  /**
   * MainClass entry point. Creates a Frame and a Style.
   * 
   * @param args Entry line arguments.
   * @throws IOException Input/Output Exception.
   */
  public static void main(String[] args) throws IOException {
    SFrame frame = new SFrame(new SEclipseStyle(), new SClassName(), new SCurlyBracket(),
        new SKeyWord(), new SMainClass(), new SModifier(), new SPseudoVariable(), new SSemiColon(),
        new SString());
    frame.pack();
    frame.setVisible(true);

    frame.addMenuItems(new SSortClassesByHierarchy(), new SSortClassesByName(),
        new SSortPackagesByName(), new SEclipseStyle(), new SBredStyle(), new SDarkStyle());

  }


}
