package sh4j.model.command;

import sh4j.model.browser.SProject;
/**
 * Class that represents the sorting command, sort packages by name.
 * @author sircrab
 *
 */
public class SSortPackagesByName extends SCommand {

  @Override
  public void executeOn(SProject project) {
    SIVisitor visitor = new SSortPackagesByNameVisitor();
    visitor.visit(project);

  }

}
