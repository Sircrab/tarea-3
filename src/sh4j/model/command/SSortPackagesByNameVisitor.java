package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
/**
 * Visitor which sorts packages lexicograpically.
 * @author sircrab
 *
 */
public class SSortPackagesByNameVisitor implements SIVisitor {

  @Override
  public void visit(SClass obj) {
    obj.accept(this);

  }

  @Override
  public void visit(SPackage obj) {
    obj.accept(this);
  }

  @Override
  public void visit(SMethod obj) {
    obj.accept(this);

  }

  @Override
  public void visit(SProject obj) {
    obj.accept(this);

  }

}
