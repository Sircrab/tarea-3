package sh4j.model.command;

import sh4j.model.browser.SProject;
import sh4j.ui.IMenuItem;
import sh4j.ui.SFrame;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;


/**
 * It represent a command that could be applied to a project.
 * 
 * @author juampi
 *
 */
public abstract class SCommand implements IMenuItem {

  public abstract void executeOn(SProject project);
  /**
   * This method is used to Add this command to the menu list in the SFrame.
   */
  public void addToMenu(SFrame frame) {
    JMenuItem item = new JMenuItem(this.name());
    item.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        if (frame.project != null) {
          executeOn(frame.project);
          frame.update(frame.project);
        }
      }
    });
    frame.file.add(item);
  }

  public String name() {
    return this.getClass().getName();
  }
}
