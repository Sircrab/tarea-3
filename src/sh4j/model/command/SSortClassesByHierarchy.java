package sh4j.model.command;



import sh4j.model.browser.SProject;

/**
 * Class that represent the sorting command sort by hierarchy.
 * 
 * @author sircrab
 *
 */
public class SSortClassesByHierarchy extends SCommand {

  @Override
  public void executeOn(SProject project) {
    SIVisitor previsitor = new SSortClassesByNameVisitor();
    SIVisitor visitor = new SSortClassesByHierarchyVisitor();
    previsitor.visit(project);
    visitor.visit(project);

  }

}
