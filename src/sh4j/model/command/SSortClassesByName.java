package sh4j.model.command;

import sh4j.model.browser.SProject;
/**
 * Class which represents the sorting command, sort classes by name.
 * @author sircrab
 *
 */
public class SSortClassesByName extends SCommand {

  @Override
  public void executeOn(SProject project) {
    SIVisitor visitor = new SSortClassesByNameVisitor();
    visitor.visit(project);

  }

}
