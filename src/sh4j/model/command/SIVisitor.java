package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
/**
 * Interface for a visitor and the objects it can visit.
 * @author sircrab
 *
 */
public interface SIVisitor {
  void visit(SClass obj);

  void visit(SPackage obj);

  void visit(SMethod obj);

  void visit(SProject obj);

}
