package sh4j.parser;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import sh4j.model.browser.SMethod;
import sh4j.model.browser.SPrivateMethod;
import sh4j.model.browser.SProtectedMethod;
import sh4j.model.browser.SPublicMethod;

public class SMethodParser extends ASTVisitor {
  private List<SMethod> methods;

  public SMethodParser() {
    methods = new ArrayList<SMethod>();
  }

  public boolean visit(MethodDeclaration node) {
    SASTParser parser = new SASTParser();
    node.accept(parser);
    SMethod candidate = new SMethod(node, parser.top());
    String modifiers = candidate.modifier();
    if (modifiers.contains("public")) {
      methods.add(new SPublicMethod(node, parser.top()));
    } else if (modifiers.contains("private")) {
      methods.add(new SPrivateMethod(node, parser.top()));
    } else if (modifiers.contains("protected")) {
      methods.add(new SProtectedMethod(node, parser.top()));
    } else {
      methods.add(candidate);
    }
    return super.visit(node);
  }

  public List<SMethod> methods() {
    return methods;
  }
}
